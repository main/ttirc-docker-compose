### Reverse proxy example

(Various buffering-related stuff is needed for EventSource / Server-Sent-Events to work properly).

```nginx
	location ^~ /tt-irc/ {
		client_max_body_size 50M;
	
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header X-Forwarded-Proto $scheme;

		proxy_buffering off;
		proxy_request_buffering	off;

		proxy_set_header Connection '';
		proxy_http_version 1.1;
		chunked_transfer_encoding off;

		proxy_pass http://127.0.0.1:8280/tt-irc/;
		break;
	}
```

### Timezone issues

This is not yet handled automatically on startup; add the following to `config.php` to synchronize database and PHP timezones:

```
   define('DB_TIMEZONE', 'Europe/Moscow'); # your favorite timezone
   date_default_timezone_set(DB_TIMEZONE); # important part
```