#!/bin/sh -e

while ! pg_isready -h $DB_HOST -U $DB_USER; do
	echo waiting until $DB_HOST is ready...
	sleep 3
done

if ! id app >/dev/null 2>&1; then
	addgroup -g $OWNER_GID app
	adduser -D -h /var/www/html -G app -u $OWNER_UID app
fi

DST_DIR=/var/www/html/tt-irc
SRC_REPO=https://dev.tt-rss.org/archive/tt-irc.git

[ -e $DST_DIR ] && rm -f $DST_DIR/.app_is_ready

export PGPASSWORD=$DB_PASS

[ ! -e /var/www/html/index.php ] && cp /index.php /var/www/html

PSQL="psql -q -h $DB_HOST -U $DB_USER $DB_NAME"

if [ ! -d $DST_DIR/.git ]; then
	mkdir -p $DST_DIR
	echo cloning tt-irc source from $SRC_REPO to $DST_DIR...
	git clone $SRC_REPO $DST_DIR || echo error: failed to clone master repository.
else
	echo updating tt-irc source in $DST_DIR from $SRC_REPO...
	cd $DST_DIR && \
		git config core.filemode false && \
		git pull origin master || echo error: unable to update master repository.
fi

if [ ! -e $DST_DIR/index.php ]; then
	echo "error: tt-irc index.php missing (git clone failed?), unable to continue."
	exit 1
fi

RESTORE_SCHEMA=$DST_DIR/backups/restore-schema.sql.gz

if [ -r $RESTORE_SCHEMA ]; then
	zcat $RESTORE_SCHEMA | $PSQL
	find $DST_DIR/cache -type f -delete
elif ! $PSQL -c 'select * from ttirc_version'; then
	$PSQL < /var/www/html/tt-irc/schema/ttirc_schema_pgsql.sql
fi

chown -R $OWNER_UID:$OWNER_GID $DST_DIR \
	/var/log/php7

for d in cache lock; do
	chmod 777 $DST_DIR/$d
	find $DST_DIR/$d -type f -exec chmod 666 {} \;
done

SELF_URL_PATH=$(echo $SELF_URL_PATH | sed -e 's/[\/&]/\\&/g')

if [ ! -s $DST_DIR/config.php ]; then
	sed \
		-e "s/define('DB_HOST'.*/define('DB_HOST', '$DB_HOST');/" \
		-e "s/define('DB_USER'.*/define('DB_USER', '$DB_USER');/" \
		-e "s/define('DB_NAME'.*/define('DB_NAME', '$DB_NAME');/" \
		-e "s/define('DB_PASS'.*/define('DB_PASS', '$DB_PASS');/" \
		-e "s/define('DB_TYPE'.*/define('DB_TYPE', 'pgsql');/" \
		-e "s/define('DB_PORT'.*/define('DB_PORT', 5432);/" \
		-e "s/define('REDIS_SERVER'.*/define('REDIS_SERVER', '$REDIS_SERVER:6379');/" \
		< $DST_DIR/config.php-dist > $DST_DIR/config.php

	cat >> $DST_DIR/config.php << EOF
		define('_NGINX_XACCEL_PREFIX', '/tt-irc');
EOF
fi

touch $DST_DIR/.app_is_ready

sudo -u app /usr/sbin/php-fpm7 -F

